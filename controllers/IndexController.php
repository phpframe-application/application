<?php

namespace App\Cms\Controllers;
use App\Cms\Http\Request;

/**
 *
 */
class IndexController extends CmsController
{
  public function index()
  {
    return $this->view('index', []);
  }
}
