<?php

namespace App\Cms\Controllers;

return [
  'admin' => [ 'method' => 'ANY', 'url' => '/admin', 'controller' => [ IndexController::class, 'index' ] ]
];
